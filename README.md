# LX_RoCoN/PXMC/MARS8 Commander

Collection of scripts for communication, monitoring
and commanding motion and robotic controllers based
on Portable, highly eXtendable Motion Control library
[PXMC.org](https://pxmc.org/) conceived by [PiKRON.com](http://pikron.com/)
company.

The actual target of this project is control of Phytron VSS 32.200.1.2
stepper motor by PiKRON [LX_RoCoN](http://pikron.com/pages/products/motion_control/lx_rocon.html)
unit in ESA project preliminary ground experiments. But there exists
more derivates of Python based control software for PXMC based units.
One example has been done for modified LX_RoCoN firmware used for
experiments in area of model predictive control another is
[PyRoCoN](https://github.com/cvut/pyrocon) package ued for
[robotic education](https://cw.fel.cvut.cz/wiki/help/common/robot_crs) at Czech Technical University in Prague (see [demo with BOSCH scara kinematc robot](http://www.pikron.com/media/bosch-roses-demo.mp4)).

## LXR LISA Commander

Se [README.md](lxr-lisa-com/README.md) in lxr-lisa-com directory
