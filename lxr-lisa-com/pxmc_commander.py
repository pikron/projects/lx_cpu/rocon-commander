#!/usr/bin/python3
#
# Commander for PiKRON's PXMC.org Based Motion Control Units
#
# Copyright (c) 2014-2023 Pavel Pisa <ppisa@pikron.com>
# PiKRON s.r.o. http://www.pikron.com/
# Based on:
#   - blueb_commander.py for MARS8 units (C) 2014 Pavel Pisa
#   - gpc_commander.py for Explicit Model Predictive Control of PMSM Drives
#     https://doi.org/10.1109%2FISIE45552.2021.9576296
#   - pyRoCoN https://github.com/cvut/pyrocon project
#     developed under PiKRON s.r.o funding
#       Copyright (c) 2017 Olga Petrova <olga.petrova@cvut.cz>
#       Advisor: Pavel Pisa <pisa@cmp.felk.cvut.cz>
#       FEE CTU Prague, Czech Republic
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

''' Module provides communication functions. '''

import sys
import serial
import socket
import time
import numpy as np
import argparse

def degtorad(d):
    """
    Convert degrees to radians.
    :param d: Angle in degrees.
    :return: Angle in radians.
    """
    return d * np.pi / 180.0;

def radtodeg(d):
    """
    Convert radians to degrees.
    :param d: Angle in radians.
    :return: Angle in degrees.
    """
    return d * 180.0 / np.pi;

class pxmc_commander(object):
    def __init__(self, rcon = None, rpar = None, riku = None):
        """
        Commander constructor.
        :param robot: Robot instance, e.g. robotBosch, robCRS97 or robCRS93.
        :param rcon: Serial interface.
        """
        self.rpar = rpar
        self.riku = riku
        self.rcon = rcon
        self.hh_axes_list=''
        self.control_axes_list=''
        self.coord_axes = ''
        self.stamp = int(time.time()%0x7fff)
        self.last_trgt_irc = None
        self.coordmv_commands_to_next_check = 0
        self.coordmv_queue_check_skip = 20

    def set_hh_axes_list(self, axes_list):
        self.hh_axes_list = axes_list

    def set_control_axes_list(self, axes_list):
        self.control_axes_list = axes_list

    def set_coord_axes_list(self, axes_list):
        self.coord_axes_list = axes_list

    def set_rcon(self, rcon):
        """
        Set communication interface.
        :param rcon: Serial interface.
        """
        if self.rcon is not None:
            self.rcon.close()
            self.rcon = None
        self.rcon = rcon

    def send_cmd(self, cmd):
        """
        Send command to command unit through serial interface.
        :param cmd: Command to send.
        """
        #print(cmd)
        ba = bytearray(cmd, 'ascii')
        self.rcon.write(ba)

    def read_resp(self, maxbytes):
        """
        Read response from command unit.
        :param maxbytes: Max number of bytes to read.
        """
        resp = self.rcon.read(maxbytes)
        if resp is None:
            return None
        s = resp.decode("ascii")
        s = s.replace("\r\n", "\n")
        s = s.replace("\r", "\n")
        return s

    def sync_cmd_fifo(self):
        """
        Synchronize message queue.
        """
        self.stamp = (self.stamp + 1) & 0x7fff
        self.send_cmd('STAMP:%d\n' % self.stamp)
        buf='\n'
        while True:
            buf += self.read_resp(1024)
            i = buf.find('\n' + 'STAMP=')
            if i < 0:
                continue
            s = '%d'%self.stamp
            r = buf[i+7:]
            j = r.find('\n')
            if j < 0:
                continue
            r = r[0:j].strip()
            if r == s:
                break

    def check_ready(self, for_coordmv_queue = False):
        """
        Check robot is in "ready" state.
        :param for_coordmv_queue: Boolean, whether to check state for coordinate movement message queue.
        :return: Boolean, whether robot is ready or not.
        """
        a = int(self.query('ST'))
        s = ''
        if a & 0x8:
            s = 'error, '
        if a & 0x10000:
            s = s + 'arm power is off, '
        if a & 0x20000:
            s = s + 'motion stop, '
        if s:
            raise Exception('Check ready:' + s[:-2] + '.')
        if for_coordmv_queue:
            return False if a & 0x80 else True
        else:
            return False if a & 0x10 else True

    def init_communication(self):
        """
        Initialize communication through serial interface.
        """
        s = self.read_resp(1024)
        self.send_cmd("\nECHO:0\n")
        self.sync_cmd_fifo()
        s = self.query('VER')
        print('Firmware version : ' + s)

        # Purge
        self.send_cmd("PURGE:\n")
        s = self.read_resp(2000)
        print(s);

    def set_int_param(self, param, val):
        """
        Set global integer parameter
        :param param: Parameter to set.
        :param val: Value of parameter.
        """
        valstr = str(int(val))
        self.send_cmd(param + ':' + valstr + '\n')

    def set_int_param_for_axes(self, param, val, axes_list = None):
        """
        Set parameter for a joint.
        :param param: Parameter to set.
        :param val: Value of parameter.
        :param axes_list: List of joints to set parameters to.
        """
        if axes_list is None:
            axes_list = self.control_axes_list
        valstr = str(int(val))
        for a in axes_list:
            self.send_cmd(param + a + ':' + valstr + '\n')
            #print (param+a+':'+valstr+'\n')

    def set_max_speed(self, val, axes_list = None):
        """
        Set maximal speed of joints.
        :param val: Maximal speeds for joints in axes_list.
        :param axes_list: List of joints to set maximal speeds to.
        """
        self.set_int_param_for_axes(axes_list = axes_list, param = 'REGMS', val = val)

    def setup_coordmv(self, axes_list=None):
        """
        Setup coordinate movement of joints in axes_list.
        :param axes_list: List of joints.
        """
        if axes_list is None:
            axes_list = self.coord_axes
        self.wait_ready()
        axes_coma_list = ','.join(axes_list)
        print(axes_coma_list, axes_list)
        self.send_cmd('COORDGRP:' + axes_coma_list + '\n')
        print('COORDGRP:', axes_coma_list)
        self.wait_ready()
        self.coord_axes = axes_list
        self.last_trgt_irc = None

    def setup_coordmv_queue_size(self, size = None, warn = None):
        if size is not None:
           self.set_int_param('COORDQUEMAX', size)
        if warn is not None:
           self.set_int_param('COORDQUWARN', warn)
        coord_warn =  self.query_int('COORDQUWARN')
        coord_warn = int(coord_warn)
        if coord_warn >= 20:
           self.coordmv_queue_check_skip = coord_warn - 10
        else:
           self.coordmv_queue_check_skip = coord_warn / 2

    def throttle_coordmv(self):
        """
        Throttle message queue for coordinate movement.
        :return: Boolean whether the queue is throttled.
        """
        throttled = False
        if self.coordmv_commands_to_next_check <= 0:
            self.coordmv_commands_to_next_check = self.coordmv_queue_check_skip
        else:
            self.coordmv_commands_to_next_check -= 1
            return
        while not self.check_ready(for_coordmv_queue = True):
            if not throttled:
                print('coordmv queue full - waiting')
            throttled = True
        return throttled

    def coordmv(self, pos, min_time=None, relative=False):
        """
        Coordinate movement of joints.
        :param pos: Position to move to.
        :param min_time: Minimal time for the movement, if None movement is carried in minimal possible time.
        :param relative: Boolean, whether movement is relative to previous (current) position.
        :param disc: Discontinuity of movement, internal parameter, is to be found in control unit docs.
        """
        self.throttle_coordmv()
        cmd = 'COORDMV' if not relative else 'COORDRELMVT'
        if (min_time is not None) and not relative:
            cmd += 'T'
        cmd += ':'
        if (min_time is not None) or relative:
            if min_time is None:
                min_time=0
            cmd += str(int(round(min_time)))
            if len(pos) > 0:
                cmd += ','
        pos = [int(round(p)) for p in pos]
        cmd += ','.join([str(p) for p in pos])
        print('cmd', cmd)
        self.send_cmd(cmd + '\n')
        if relative:
            if self.last_trgt_irc is None:
                return
            pos = [p + t for p,t in zip(pos, self.last_trgt_irc)]
            # pos = map(int.__add__, pos, self.last_trgt_irc)
        self.last_trgt_irc = pos

    def axis_get_pos(self, axis_lst=None):
        """
        Get position of joints.
        :return: Position of active joints.
        """
        if axis_lst is None:
            axis_lst = self.robot.coord_axes
        resp = self.query('COORDAP')
        try:
            resp = np.array(map(int,resp.split(',')))
        except:
            print('Error responce', resp)
        t = resp[0]
        pos = resp[-6:]
        return t, pos

    def hard_home(self, axes_list = None):
        """
        Robot hard homing. Returns joints in axes_list to home position.
        :param axes_list: List of joints to home.
        """
        if axes_list is None:
            axes_list = self.hh_axes_list
        for a in axes_list:
            self.send_cmd('HH'+a+':\n')
        self.wait_ready()
        for a in axes_list:
            self.send_cmd('HH'+a+':\n')
            self.wait_ready()

    def query(self, query):
        """
        Send query to control unit.
        :param query: Query to send.
        :return: Control unit's response.
        """
        buf = '\n'
        self.send_cmd('\n' + query + '?\n')
        while True:
            buf += self.read_resp(1024)
            i = buf.find('\n' + query + '=')
            if i < 0:
                continue
            j = buf[i+1:].find('\n')
            if j != -1:
                break
        if buf[i+1+j-1] == '\r':
            j -= 1
        res = buf[i+2+len(query):i+1+j]
        return res

    def query_int(self, query):
        """
        Send query to control unit for integer parameter.
        :param query: Query to send.
        :return: Control unit's response or None.
        """
        s = self.query(query)
        if s is None or len(s) == 0:
            raise Exception('Query ' + query + ': without response.')
        return int(s)

    def query_int_csv(self, query):
        """
        Send query to control unit for integer parameter.
        :param query: Query to send.
        :return: Control unit's response or None.
        """
        s = self.query(query)
        if s is None or len(s) == 0:
            raise Exception('Query ' + query + ': without response.')
        return [int(e) for e in s.split(',')]

    def command(self, command, *args):
        """
        Send command to control unit.
        :param command: Command to send
        """
        self.send_cmd(command + ':' + ','.join(map(str, args)) + '\n')

    def command_for_axes(self, command, axes_list, *args):
        """
        Set command to the list of axes or default list if None specified.
        :param command: Command to send
        :param axes_list: List of joints to set parameters to.
        """
        if axes_list is None:
            axes_list = self.control_axes_list
        for a in axes_list:
            self.send_cmd(command + a + ':' + ','.join(map(str, args)) + '\n')

    def wait_ready(self, sync=False):
        """
        Wait for control unit to be ready.
        :param sync: Boolean, whether to synchronize with control unit.
        """
        buf = '\n'
        if sync:
            self.sync_cmd_fifo()
            print('Synchronized!')
        self.send_cmd("\nR:\n")
        while True:
            buf += self.read_resp(1024)
            if buf.find('\nR!') >= 0:
                return True
            if buf.find('\nFAIL!') >= 0:
                return False

    def release(self):
        """
        Release errors and reset control unit.
        """
        self.send_cmd("RELEASE:\n")

    def set_axis_mode(self, mode, axes_list, params = None):
        self.command_for_axes("RELEASE", axes_list)
        self.command_for_axes("PURGE", axes_list)

        if params is not None:
            for par_tup in params:
                self.set_int_param_for_axes(par_tup[0], par_tup[1], axes_list)

        if mode is not None:
            self.set_int_param_for_axes("REGMODE", mode, axes_list)

        self.set_int_param_for_axes("PWM", 0, axes_list)

    def check_axis_mode(self, mode, axes_list, params = None):
        if axes_list is None:
            axes_list = self.control_axes_list
        for a in axes_list:
            val = self.query_int("REGMODE" + a)
            if val != mode:
                return False
            for p in params:
                val = self.query_int(p[0] + a)
                if val != p[1]:
                    return False
        return True;

def rcon_serial(tty_dev, rtscts = False):
    ser = serial.Serial(tty_dev,
                    baudrate = 115200,
                    bytesize = serial.EIGHTBITS,
                    parity = serial.PARITY_NONE,
                    stopbits = serial.STOPBITS_ONE,
                    rtscts = rtscts,
                    timeout = 0.1)
    ser.reset_input_buffer()
    return ser

class rcon_socket_rw(socket.socket):
    def __init__(self, *args, **kwargs):
        super(rcon_socket_rw, self).__init__(*args, **kwargs)

    def read(self, size=1):
        try:
            self.settimeout(0.01)
            data = self.recv(size)
            self.settimeout(5)
        except socket.timeout:
            data = bytearray()
        return data

    def write(self, data):
        return self.sendall(data)

def rcon_soc_tcp(host_name = 'rocon.local'):
    s = rcon_socket_rw(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(5)
    s.connect((host_name, 23))
    s.write(bytearray('rocon\n', 'ascii'))
    s.write(bytearray('pikron\n', 'ascii'))
    return s

def pxmc_setup_stepper_motor(r, axes_list, reg_mode, reg_params):
        r.command("RELEASE")
        r.command("PURGE")
        r.command("REGOUTMAP",0,4,8,12)

        r.set_control_axes_list(axes_list)
        r.set_coord_axes_list(axes_list)

        r.set_axis_mode(reg_mode, None, reg_params)

        if not r.check_axis_mode(reg_mode, None, reg_params):
            print('Parameters check failed')
            r.command("RELEASE")
            sys.exit(1)

if __name__ == '__main__':
    help_msg = '''SYNOPSIS: pxmc_commander.py [-d /dev/ttyXXX] [-H] [-A AB..]
                Control PXMC based moton control systems'''

    parser = argparse.ArgumentParser(description='BlueBot robot commander')
    parser.add_argument('-s', '--skip-setup', dest='skip_setup', action='store_true',
                        default=False, help='skip hard-home inicialization of robot')
    parser.add_argument('-d', '--tty-device', dest='tty_dev', type=str,
                        default=None, help='tty line/device to robot')
    parser.add_argument('-H', '--hw-flow-ctrl', dest='tty_rtscts', action='store_true',
                        default=False, help='tty HW flow control, RTS/CTS')
    parser.add_argument('-n', '--host-name', dest='host_name', type=str,
                        default='rocon.local', help='host name for TCP communication')
    parser.add_argument('-m', '--max-speed', dest='max_speed', type=int,
                        default=None, help='maximal motion speed')
    parser.add_argument('-t', '--reg-type', dest='reg_type', type=int,
                        default=None, help='controller type selection')
    parser.add_argument('-P', '--reg-p', dest='reg_p', type=int,
                        default=None, help='controller P constant')
    parser.add_argument('-I', '--reg-i', dest='reg_i', type=int,
                        default=None, help='controller I constant')
    parser.add_argument('-D', '--reg-d', dest='reg_d', type=int,
                        default=None, help='controller D constant')
    parser.add_argument('--reg-s1', dest='reg_s1', type=int,
                        default=None, help='controller S1 constant')
    parser.add_argument('--reg-s2', dest='reg_s2', type=int,
                        default=None, help='controller S2 constant')
    parser.add_argument('-T', '--reg-preset', dest='reg_preset', action='store_true',
                        default=False, help='skip hard-home inicialization of robot')
    parser.add_argument('-A', '--axes-list', dest='axes_list', type=str,
                        default='A', help='list of axes to apply command')
    parser.add_argument('-p', '--target-position', dest='target_position', type=int,
                        default=None, help='target position')
    parser.add_argument('-a', '--action', dest='action', type = str,
                        default='stepping', help='action to run (i.e. stepping, steppingbyspdfg, release, reboot)')
    parser.add_argument('--step-count', dest='step_count', type=int,
                        default=200, help='number of steps to proceed')
    parser.add_argument('--step-fraction', dest='step_fraction', type=int,
                        default=1, help='how much to divide step')
    parser.add_argument('--step-delay', dest='step_delay', type=float,
                        default=1, help='time between steps in seconds')

    args = parser.parse_args()

    tty_rtscts = args.tty_rtscts
    tty_dev = args.tty_dev
    host_name = args.host_name
    skip_setup = args.skip_setup
    max_speed = args.max_speed
    reg_type = args.reg_type
    reg_p = args.reg_p
    reg_i = args.reg_i
    reg_d = args.reg_d
    reg_s1 = args.reg_s1
    reg_s2 = args.reg_s2
    axes_list = args.axes_list.upper()
    action = args.action

    step_count = args.step_count
    step_fraction = args.step_fraction
    step_delay = args.step_delay
    axes_list = args.axes_list.upper()

    if args.reg_preset:
        if reg_type is None:
            reg_type = 3

    if action is None:
        action == 'cyclic'

    r = pxmc_commander();

    if tty_dev is not None:
        r.set_rcon(rcon_serial(tty_dev, rtscts = tty_rtscts))
    elif host_name is not None:
        r.set_rcon(rcon_soc_tcp(host_name = host_name))
    else:
        print("Any method to specify connection the the device specified")
        exit(1);


    #ser.open()

    #ser.write("SPIMST0:4(%02X,%02X)\n"%((output_data>>8)&0xFF,(output_data)&0xFF))
    #s = ser.read(1000)
    #print(s)

    r.init_communication()

    if action == 'reboot':
        r.command("REBOOT")
        time.sleep(2)
        exit(0)

    if action == 'release':
        r.command("RELEASE")
        r.command("PURGE")
        r.sync_cmd_fifo()
        exit(0)

    # parameters for Phytron VSS 32.200.1.2
    reg_mode = 8
    reg_params = [
        # Winding current controller
        ("REGCURDP", 50),    # proportional constant for direct component
        ("REGCURDI", 5000),  # integrative constant for direct component
        ("REGCURQP", 50),    # proportional constant for qudrature component
        ("REGCURQI", 5000),  # integrative constant for qudrature component
        ("REGCURHOLD", 450),  # holding current 1 A
        ("REGME", 1000),      # maximal command/volatage not used without feedback
        ("REGPTPER", 1),      # the increments are conuted per single period
        ("REGPTMARK", 0),     # no syncronization from IRC mark or index used
        ("REGACC", 1)         # maximal accelelaration in single sampling period
        # not set directly there, will be added according to use
        #("REGPTIRC", 1280),   # increments per REGPTPER eletrical cycles/revolutions/full-steps
    ]

    if action == 'steppingbyspdfg':
        if len(axes_list) != 1:
            print('exactly one axis to control is required for steppingbyspdfg')
            exit(1)

        irc_per_step = step_fraction
        reg_params.append(("REGPTIRC", irc_per_step * 4))

        pxmc_setup_stepper_motor(r, axes_list, reg_mode, reg_params)

        step_ticks = step_delay * 4000

        r.wait_ready(True)

        start_time = time.time()

        r.set_int_param_for_axes("REGMS", irc_per_step*256, axes_list)
        r.set_int_param_for_axes("REGACC", irc_per_step*256, axes_list)

        spdfg_val = int(round(0x1000000 / step_ticks))
        spdfg_timeout = int(round(0x1000000 / spdfg_val * abs(step_count))) + 2
        if step_count < 0:
            spdfg_val = -spdfg_val

        #r.send_cmd('testlxpwrrx 16\n')
        r.command_for_axes('SPDFGT', axes_list, spdfg_val, spdfg_timeout)

        r.wait_ready(True)
        stop_time = time.time()
        print("Run time of script from PC: " +  str(stop_time - start_time))

        exit(0)

    # the full step is one quarter or eletrical revolution
    irc_per_step = 1280 // 4

    reg_params.append(("REGPTIRC", irc_per_step * 4))

    if True:
        pxmc_setup_stepper_motor(r, axes_list, reg_mode, reg_params)

        # align location when started the first
        for a in axes_list:
            ap = r.query_int("AP" + a)
            print(ap)
            if ap >= -2 and ap < 4:
                r.command_for_axes("G", a, 0)
            else:
                r.command_for_axes("GR", a, 0)

    if (action == 'stepping') and (len(axes_list) > 0):

       step_ticks = step_delay * 4000

       r.setup_coordmv(axes_list)

       r.set_int_param_for_axes("REGMS", irc_per_step*256, axes_list)
       r.set_int_param_for_axes("REGACC", irc_per_step*256, axes_list)
       r.set_int_param('COORDDISCONT', 2)
       r.set_int_param('CMDTIMEMASK',2147483647)
       r.set_int_param('COORDSQNMASK',2147483647)
       r.setup_coordmv_queue_size(4000, 3000)

       step_irc = np.ones(len(axes_list), dtype = int) * (irc_per_step // step_fraction)

       if step_count < 0:
          step_count = -step_count
          step_irc = -step_irc

       time_reminder = 0.0

       r.wait_ready(True)

       start_cap = r.query_int_csv('COORDAP')

       start_time = time.time()

       #r.send_cmd('testlxpwrrx 16\n')

       for i in range(0, step_count):
           r.coordmv(step_irc, min_time=0, relative=True)
           wait_time = time_reminder + step_ticks - 3
           wait_time_int = int(round(wait_time))
           if wait_time_int < 0:
               wait_time_int = 0
           time_reminder = wait_time - wait_time_int
           r.coordmv(step_irc * 0, min_time=wait_time_int, relative=True)

       r.wait_ready(True)
       stop_time = time.time()

       stop_cap = r.query_int_csv('COORDAP')

       runtime_by_lxr = stop_cap[0] - start_cap[0]
       if runtime_by_lxr < 0:
           runtime_by_lxr += 2147483648

       segments_by_lxr = stop_cap[1] - start_cap[1]
       if segments_by_lxr < 0:
           segments_by_lxr += 2147483648

       print('Run time of ' + str(segments_by_lxr / 2) + ' segments from LX_RoCoN: ' +  str(runtime_by_lxr / 1000))
       print("Run time of script from PC: " +  str(stop_time - start_time))
