# LXR LISA Commander

The Python scrips to communicate with [PiKRON.com](http://pikron.com/) [LX_RoCoN](http://pikron.com/pages/products/motion_control/lx_rocon.html)
based on [PXMC.org](https://pxmc.org/) motion control system.
In the particular case, it is used with Phytron VSS 32.200.1.2
stepper motor.

## Command Line Options

 usage: pxmc_commander.py [-h] [-s] [-d TTY_DEV] [-H] [-n HOST_NAME]

### Description

usage: pxmc_commander.py [-h] [-s] [-d TTY_DEV] [-H] [-n HOST_NAME] [-a ACTION]   [--step-count STEP_COUNT] [--step-fraction STEP_FRACTION] [--step-delay STEP_DELAY]

Control PXMC based moton control systems

### Optional Arguments for Typical Use:

* **-h**, **--help**
  show this help message and exit
* **-d** TTY_DEV, **--tty-device** TTY_DEV
  tty line/device to robot, /dev/ttyUSB0 or /dev/ttyACM0 on GNU/Linux and COMxx on Windows
* **-H**, **--hw-flow-ctrl**
  tty HW flow control, RTS/CTS
* **-n** HOST_NAME, **--host-name** HOST_NAME
  host name for TCP communication, the default is 'rocon.local' but IPv4 address can be specified as well
* **-A** AXES_LIST, **--axes-list** AXES_LIST
  list of axes to apply command, for actual application 'A', 'B' or 'AB'
* **-a** ACTION, **--action** ACTION
  action to run
  * "stepping" .. default action for lxr-lisa-com version
  * "steppingbyspdfg" .. implement stepping by fine-grained speed command
  * "release" .. release motors/stop winding currents
  * "reboot" .. reset/reboot the unit
* **--step-count** STEP_COUNT
  number of steps to proceed, negative values switches direction
* **--step-fraction** STEP_FRACTION
  how much to divide step, expected 1 for full stepping and 16 for microstepping
* **--step-delay** STEP_DELAY
  time between steps in seconds

## Examples

```
pxmc_commander.py -A 'A' --step-count 100 --step-fraction=16 --step-delay=0.1
```

Connect to the rocon by default TCP/IP address, run 100 1/16 micro-steps at 10 Hz frequency (0.1 s)
